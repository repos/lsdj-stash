#!/bin/sh

EMS_USER="a"
COMMAND="/home/a/v/borok-pub/lsdj-stash/utils/autodump/autodump.sh&"
SAVE_DIRECTORY="/home/a/v/borok-pub/lsdj-stash/a/tmp"

if [ "$(whoami)" = "root" ]
then
  su ${EMS_USER} -c ${COMMAND}
fi

if [ "$(whoami)" = "${EMS_USER}" ]
then
  dunstify "dumping SAVE file, plz wait..."
  ems-flasher --save --dump /tmp/LSDj.sav && dunstify "done"
  if ! [ -e  "/tmp/LSDj.sav" ]
  then
    dunstify "something went wrong..."
    exit 1
  fi
  LSDJ_VERSION="$(ems-flasher --title | grep LSDj | cut -d ' ' -f 5)"
  SAVE_FILENAME="${LSDJ_VERSION}-$(date '+%y%m%d-%H:%M').sav"
  mv /tmp/LSDj.sav ${SAVE_DIRECTORY}/${SAVE_FILENAME}
  sleep 1s && dunstify "${SAVE_FILENAME} dumped!"
fi

